/*
 * Global.h
 *
 *  Created on: May 24, 2016
 *      Author: user
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_
#include <string>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <sstream>
#include <vector>

using namespace std;
namespace npl {

class Global {
public:

	Global();

	virtual ~Global();

	static string convertTostring(int i){
		string Result;
		ostringstream convert;
		convert << i;
		Result = convert.str();
		return Result;
	}

	static string* splitString(string data){
		bool flag=false;
		string * myStr = new string[2];
		string str;
		istringstream iss(data);
			while (iss){
				string temp;
				iss>>temp;
				if(flag == false){
					myStr[0]= temp;
					flag=true;
				}
				else{
				str.append(temp);
				}
			}
			myStr[1]=str;
			return myStr;
	}

	static void printMap(map<string, string> data){
		cout<<"{"<<endl;
		for (map<string, string>::iterator it = data.begin();
					it != data.end(); it++) {
				cout << "   " << it->first <<" "<< it->second<< endl;
			}
		cout<<"}"<<endl;
	}
};

} /* namespace npl */

#endif /* GLOBAL_H_ */
