#include "TCPSocket.h"
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>

using namespace std;
using namespace npl;

TCPSocket::TCPSocket(int connected_sock,struct sockaddr_in serverAddr,struct sockaddr_in peerAddr){
	this->serverAddr =  serverAddr;
	this->peerAddr =  peerAddr;
	socket_fd = connected_sock;
}


TCPSocket::TCPSocket(int port){
	/**
	 * socket(int domain, int type, int protocol);
	 * creates a TCP socket, returns the file descriptor
	 * AF_INET - IPv4 Internet protocols
	 * SOCK_STREAM - TCP (not udp..)
	 * 0 - default protocol type
	 */
	socket_fd = ::socket (AF_INET, SOCK_STREAM, 0);

	// clear the s_in struct
	bzero((char *) &serverAddr, sizeof(serverAddr));

	//sets the sin address
	serverAddr.sin_family = (short)AF_INET; // IPv4
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);    /* host to network long - current host*/
	serverAddr.sin_port = htons((u_short)port);

	//bind the socket on the specified address, bind to the port we set in serverAddr
	printf("TCP server - bind...\n");
	if (bind(socket_fd, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
	{
		perror ("Error binding to channel");
	}
}


TCPSocket::TCPSocket(const string& peerIp, int port){
	cout<<"Opening new client socket"<<endl;

	/**
	 * int socket(int domain, int type, int protocol);
	 * creates a TCP socket
	 * AF_INET - IPv4 Internet protocols
	 * SOCK_STREAM - TCP
	 * 0 - default protocol type
	 */
	socket_fd = ::socket (AF_INET, SOCK_STREAM, 0);

	// clear the s_in struct
	bzero((char *) &peerAddr, sizeof(peerAddr));

	//sets the sin address
	peerAddr.sin_family = (short)AF_INET;
	peerAddr.sin_addr.s_addr = inet_addr(peerIp.data());//convert the IP address in numbers-and-dots notation into a struct in_addr
	peerAddr.sin_port = htons((u_short)port);

	//int connect(int sockfd, struct sockaddr *serv_addr, int addrlen);
	if (connect(socket_fd, (struct sockaddr *)&peerAddr, sizeof(peerAddr)) < 0)
	{
		perror ("Error establishing communications");
		::close(socket_fd);
	}
}


TCPSocket* TCPSocket::listenAndAccept(){
	// second arg- number of connections allowed on the incoming queue.
	int rc = listen(socket_fd, 2);
	if (rc<0){
		return NULL;
	}
	socklen_t len = sizeof(peerAddr);
	bzero((char *) &peerAddr, sizeof(peerAddr));

	// accept returns a new socket file descriptor to use for this single connection
	int connect_sock = accept(socket_fd, (struct sockaddr *)&peerAddr, &len);
	// create a new instance to work with the new socket file descriptor
	return new TCPSocket(connect_sock,serverAddr,peerAddr);
}


int TCPSocket::recv(char* buffer, int length){
	return read(socket_fd,buffer,length);
}


int TCPSocket::send(const string& msg){
	return write(socket_fd,msg.c_str(),msg.size());
}
int TCPSocket::send(const char* buff,int len){
	return write(socket_fd,buff,len);
}


void TCPSocket::close(){
	cout<<"closing socket"<<endl;
	shutdown(socket_fd,SHUT_RDWR);//No more receptions or transmissions
	::close(socket_fd);//close and free the file descriptor
}


string TCPSocket::fromAddr(){
	//convert the IP address from in_addr to numbers-and-dots notation
	return inet_ntoa(peerAddr.sin_addr);
}

int TCPSocket::getDestPort(){
	return ntohs(peerAddr.sin_port); // network to host short
}

int TCPSocket::getSrcPort(){
	return ntohs(serverAddr.sin_port);// network to host short
}

string TCPSocket::getDestAddr(){
	//convert the IP address from in_addr to numbers-and-dots notation
	return inet_ntoa(peerAddr.sin_addr);
}

int TCPSocket::getSocket(){
	return socket_fd;
}