/*
0 * UDPClient.cpp
 *
 *  Created on: May 25, 2016
 *      Author: user
 */

#include "UDPClient.h"

namespace npl {

UDPClient::UDPClient(int port) {
	this->udpSocket = new UDPSocket(port);
	running=false;

}

UDPClient::~UDPClient() {
}


void UDPClient::run() {
	while(running){
		char buff[1024];
		int rc = udpSocket->recv(buff,sizeof(buff));
		if (rc < 0){
			cout<<"Error:Msg error "<<endl;
		}
		buff[rc] = '\0';
		cout<<">["<<findFriendName()<<"] "<<buff<<endl;
	}
}


string UDPClient::findFriendName(){
	string ip = this->udpSocket->fromAddr();
	int port = this->udpSocket->fromAddrPort();
	string Sport = Global::convertTostring(port);
	ip.append(":");
	ip.append(Sport);
	for (map<string, string>::iterator it = friendList.begin();it != friendList.end(); it++){
		string temp = it->second;
		if(ip.compare(temp)==0){
			return it->first;
		}
	}
	return NULL;
}


void UDPClient::close(){
	running=false;
}

void UDPClient::createRoomSession(map<string,string>data){
	friendList.clear();
	for(int i=0; i< (data.size()/3) ;i++){
		string iString=Global::convertTostring(i);
		string user=USER_NAME_KEY;
		user.append(iString);
		string tempIp=IP_KEY;
		tempIp.append(iString);
		string ipPort=data.find(tempIp)->second;
		string tempPort=PORT_KEY;
		tempPort.append(iString);
		ipPort.append(":");
		ipPort.append(data.find(tempPort)->second);
		friendList[data.find(user)->second]=ipPort;
	}
	if (running==false){
		running=true;
		start();
	}
}

void UDPClient::sendMsg(string msg) {
	string ipAndPort;
	string ip;
	string Sport;
	int port;
	for (map<string, string>::iterator it = friendList.begin();it != friendList.end(); it++)
	{
		ipAndPort=it->second;
		string delimiter = ":";
		string ip = ipAndPort.substr(0, ipAndPort.find(delimiter));
		string Sport = ipAndPort.substr(ipAndPort.find(delimiter) + 1);
		port=atoi(Sport.c_str());
		if(this->udpSocket->sendTo(msg,ip,port)==-1)
		cout<<"send msg failed"<<endl;
	}

}


} /* namespace npl */


