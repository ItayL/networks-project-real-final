/*
 * CLI.h
 *
 *  Created on: Jan 13, 2017
 *      Author: user
 */

#ifndef CLI_H_
#define CLI_H_

#define CONNECT_SERVER "c"
#define REGISTER_USER "register"
#define LOGIN "login"
#define PRINT_STATUS "l"
#define OPEN_CHAT "o"
#define OPEN_ROOM "or"
#define CLOSE_ROOM "cr"
#define LIST_ROOM_USERS "lru"
#define LIST_USERS "lu"
#define LIST_CONNECTED_USERS "lcu"
#define LIST_CHAT_ROOMS "lr"
#define SEND_MESSAGE "s"
#define CLOSE_SESSION "cs"
#define DISCONNECT "d"
#define HELP "help"
#define CLOSE "x"

#endif /* CLI_H_ */
