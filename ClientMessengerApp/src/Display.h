/*
 * Display.h
 *
 *  Created on: May 25, 2016
 *      Author: user
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <iostream>
#include <map>
#include <string>

using namespace std;
namespace npl {

class Display {
public:

	Display();
	void printUsersList(map<string, string> data);
	void printSessionRefused();
	void printSessionEstablished();
	void printLoginError();
	void printRegisterError();
	void printRoomUserListUpdate();
	void printARoomUserList(map<string, string> data);
	void printCommandNotFound();
	void printConnectedUsersList(map<string, string> data);
	void PrintAllRooms(map<string,string> data);
	void printExitSession();

	virtual ~Display();
};

} /* namespace npl */

#endif /* DISPLAY_H_ */


