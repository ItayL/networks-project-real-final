/*
 * Client.h
 *
 *  Created on: May 25, 2016
 *      Author: user
 */

#ifndef CLIENT_H_
#define CLIENT_H_
#include "TCPClient.h"
#include "UDPClient.h"
#include "Display.h"



// Status
#define NOT_CONNECT 1
#define CONNECT		2
#define IN_SESSION  3

using namespace std;
namespace npl {

	class Client :public TCPClient::Notify {
	private:
		TCPClient* tcpClient;
		UDPClient* udpClient;
		int status;
		Display * display;

	public:
		void onReadCommandFromServer(TCPClient* tcpClient,Command command,map<string, string> data);
		void connect(string ip);
		void listUsers();
		void listOnLineUsers();
		void listRooms();
		void listUsersInRoom(string roomName);
		void login(string UserDetials);
		void regiester(string UserDetials);
		void openSession(string userName);
		void openRoom(string roomName);
		void sendMsg(string msg);
		void currentStatus();
		void closeSession();
		void closeRoom(string roomName);
		void dissconect();
		void shutdown();
		Client();
		virtual ~Client();
	};

} /* namespace npl */

#endif /* CLIENT_H_ */
