/*
 * UDPClient.h
 *
 *  Created on: May 25, 2016
 *      Author: user
 */

#ifndef UDPCLIENT_H_
#define UDPCLIENT_H_

#include <MThread.h>
#include "UDPSocket.h"
#include <stdlib.h>
#include <string>
#include <map>
#include "Global.h"
#include "MessengerProtocol.h"

using namespace std;
namespace npl {

class UDPClient: public MThread {
	UDPSocket* udpSocket;
	bool running;
	map<string,string>friendList;

public:
	UDPClient(int port);
	void run();
	void close();
	void sendMsg(string msg);
	virtual ~UDPClient();
	void createRoomSession(map<string,string>data);
	string findFriendName();
};

} /* namespace npl */

#endif /* UDPCLIENT_H_ */
