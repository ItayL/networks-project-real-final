/*
 * Client.cpp
 *
 *  Created on: May 25, 2016
 *      Author: user
 */

#include "Client.h"

namespace npl {

Client::Client() {
	this->status = NOT_CONNECT;
	this->display = new Display();
	tcpClient = NULL;
	udpClient = NULL;
}

void Client::onReadCommandFromServer(TCPClient* tcpClient, Command command,map<string, string> data) {
	int com =command;
	if(com<0)
	{
		cout<<"Error: Error in connction to server";
		this->shutdown();
				return;
	}
	switch (command) {

	case LIST_USERS:
		this->display->printUsersList(data);
		break;

	case EXIT_SESSION:
		this->display->printExitSession();
		this->status = CONNECT;
		break;

	case ERROR_REGISTER:
		this->display->printRegisterError();
		this->status = CONNECT;
		break;

	case SESSION_REFUSED:
		this->display->printSessionRefused();
		this->status = CONNECT;
		break;

	case COMMAND_NOT_FOUND:
		this->display->printCommandNotFound();
		break;

	case ERROR_LOGIN:
		this->display->printLoginError();
		this->status = CONNECT;
		break;

	case ROOM_LIST:
		this->display->printARoomUserList(data);
		break;

	case UPDATE_ROOM:
		this->display->printRoomUserListUpdate();
		this->udpClient->createRoomSession(data);
		break;

	case ALL_ROOMS:
		this->display->PrintAllRooms(data);
		break;

	case LIST_CONNECTED_USERS:
		this->display->printConnectedUsersList(data);
		break;

	case ROOM_SESSION_ESTABLISHED:
		Global::printMap(data);
		this->display->printSessionEstablished();
		this->status=IN_SESSION;
		break;

	case SESSION_ESTABLISHED:
		this->udpClient->createRoomSession(data);
		this->display->printSessionEstablished();
		this->status=IN_SESSION;
		break;

	default:
		this->display->printCommandNotFound();
		break;
	}

}

Client::~Client() {
}

void Client::connect(string ip) {
	if (this->status == NOT_CONNECT) {
		this->tcpClient = new TCPClient(this, ip,MSNGR_PORT);

		// Read MSG
		Command command;
		map<string,string>portMap;
		MessengerProtocol::readMSG(tcpClient->getTcpSocket(),command,portMap);

		// Port Exchange
		if(command==PORT_EXCHANGE)
		{
			int intPort= atoi(portMap.find(PORT_KEY)->second.c_str());
			this->udpClient = new UDPClient(intPort);
		}

		// Change Status
		this->status = CONNECT;


		// already connected
	} else {
		cout << "Error: You are Already Connected" << endl;
	}
}

void Client::listUsers() {
	if (this->status != NOT_CONNECT)
		MessengerProtocol::SendMSG(this->tcpClient->getTcpSocket(), LIST_USERS);

	else {
		cout << "Error: You are NOT Connected" << endl;
	}
}

void Client::listOnLineUsers() {
	if (this->status != NOT_CONNECT)
		MessengerProtocol::SendMSG(this->tcpClient->getTcpSocket(),LIST_CONNECTED_USERS);

	else {
		cout << "Error: You are NOT Connected" << endl;
	}
}

void Client::listRooms() {
	if (this->status == CONNECT)
		MessengerProtocol::SendMSG(this->tcpClient->getTcpSocket(), ALL_ROOMS);

	else {
		cout << "Error: you are not connected or in session connect or press 'cs' and try again" << endl;
	}

}

void Client::listUsersInRoom(string roomName) {
	map<string, string> roomNameMap;
	roomNameMap[ROOM_NAME_KEY]=roomName;

	if (this->status != NOT_CONNECT)
		MessengerProtocol::SendMSG(this->tcpClient->getTcpSocket(),ROOM_LIST,roomNameMap);

	else {
		cout << "Error: You are NOT Connected" << endl;
	}
}

void Client::login(string UserDetials) {
	if (this->status == CONNECT){
		string delimiter = " ";
		string userName = UserDetials.substr(0, UserDetials.find(delimiter));
		string passWord = UserDetials.substr(UserDetials.find(delimiter) + 1);
		map<string, string> UserDetialsMap;
		UserDetialsMap[USER_NAME_KEY]=userName;
		UserDetialsMap[PASSWORD_KEY]=passWord;
		MessengerProtocol::SendMSG(this->tcpClient->getTcpSocket(),LOGIN,UserDetialsMap);
	}
	else{
		if (this->status == NOT_CONNECT){
			cout << "Error: You are NOT Connected" << endl;}
		else{
			cout << "Error: You are already logged in" << endl;}
		}


}

void Client::regiester(string UserDetials) {
	if (this->status == CONNECT){
		string delimiter = " ";
		string userName = UserDetials.substr(0, UserDetials.find(delimiter));
		string passWord = UserDetials.substr(UserDetials.find(delimiter) + 1);
		map<string, string> UserDetialsMap;
		UserDetialsMap[USER_NAME_KEY]=userName;
		UserDetialsMap[PASSWORD_KEY]=passWord;
		MessengerProtocol::SendMSG(this->tcpClient->getTcpSocket(),REGISTER,UserDetialsMap);
	}
	else {
		if (this->status == NOT_CONNECT){
			cout << "Error: You are NOT Connected" << endl;}
		else{
			cout << "Error: You are already logged in" << endl;}
	}

}

void Client::openSession(string userName) {
	if (this->status == CONNECT){
	map<string, string> UserDetialsMap;
	UserDetialsMap[USER_NAME_KEY]=userName;
	MessengerProtocol::SendMSG(this->tcpClient->getTcpSocket(), OPEN_SESSION,UserDetialsMap);
	}

	else {
				if (this->status == NOT_CONNECT){

					cout << "you are not connected..:)" << endl;}
				else{
					cout << "you are already in session..:)" << endl;}
				}

}

void Client::openRoom(string roomName) {
	if (this->status == CONNECT){
		map<string, string> roomNameMap;
		roomNameMap[ROOM_NAME_KEY]=roomName;
		MessengerProtocol::SendMSG(this->tcpClient->getTcpSocket(),OPEN_CHAT_ROOM,roomNameMap);
	}
	else {
			if (this->status == NOT_CONNECT){
				cout << "Error: You are NOT Connected" << endl;}
			else{
				cout << "Error: You are already logged in" << endl;}
		}

}

void Client::sendMsg(string msg) {
	if (this->status == IN_SESSION){
		this->udpClient->sendMsg(msg);}
	else
		cout << "Error: You are NOT In Session" << endl;
}

void Client::currentStatus() {
	cout << "Your status is: ";
	switch (this->status) {
	case 1:
		cout << "NOT CONNECT" << endl;
		break;
	case 2:
		cout << "CONNECT" << endl;
		break;
	case 3:
		cout << "IN SESSION" << endl;
		break;
	default:
		break;
	}
}

void Client::closeSession() {
	if (this->status == IN_SESSION){
		MessengerProtocol::SendMSG(this->tcpClient->getTcpSocket(), EXIT_SESSION);
		this->status = CONNECT;
		this->udpClient->close();
	}
	else
		cout << "Error: You are NOT In Session" << endl;
}

void Client::closeRoom(string roomName) {
	if (this->status != NOT_CONNECT){
		MessengerProtocol::SendMSG(this->tcpClient->getTcpSocket(), CLOSE_ROOM);
		this->status = CONNECT;
	}
	else
		cout << "Error: You are NOT Connected" << endl;
}

void Client::dissconect() {
	if(status==IN_SESSION)
		this->udpClient->close();
	if (this->status != NOT_CONNECT){
		MessengerProtocol::SendMSG(this->tcpClient->getTcpSocket(), DISCONNECT);
	this->status = NOT_CONNECT;
	}
	else
		cout << "Error: You are NOT Connected" << endl;

}

void Client::shutdown() {
	if (this->status == IN_SESSION) {
		this->closeSession();
	}
	if (this->status == CONNECT) {
		this->dissconect();
	}
	this->status = NOT_CONNECT;
	this->udpClient->close();
	this->tcpClient->close();
	this->display->~Display();
}


} /* namespace npl */

