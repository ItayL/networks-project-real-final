/*
 * CLI.h
 *
 *  Created on: Jan 13, 2017
 *      Author: user
 */
#include <iostream>
#include <string.h>
#include "Client.h"
#include "Global.h"
#include "CLI.h"

using namespace std;
using namespace npl;

static void printInstructions() {
		cout<<"---------------MESSAGER CLIENT-----------------------"<<endl;
		cout << "c <server ip>                    - Connect Server" << endl;
		cout << "register <user name> <password>  - Register User"<< endl;
		cout << "login <user name> <password>     - Login" << endl;
		cout << "l                                - Print Status" << endl;
		cout << "o <user>                         - Open Chat with user" << endl;
		cout << "or <roomName>                    - Open Room" << endl;
		cout << "cr <roomName>                    - Delete Room" << endl;
		cout << "lru <roomName>                   - List Users in Room" << endl;
		cout << "lu                               - List Registered Users" << endl;
		cout << "lcu                              - List Connected Users" << endl;
		cout << "lr                               - List Chat Rooms" << endl;
		cout << "s <message>                      - Send Message"<< endl;
		cout << "cs                               - Close Session" << endl;
		cout << "d                                - Disconnect" << endl;
		cout << "help                             - Print Help Menu" << endl;
		cout << "x                                - Quit" << endl;
		cout << "--------------------------------------------------------" << endl;

}

int main() {
	string DELIMITER = " ";

	Client* client = new Client();
	printInstructions();
	while (true) {

		string command;
		cout <<"Enter Command:"<<endl;
		getline(cin,command);
		string beginCommand = command.substr(0, command.find(DELIMITER));
		string endCommand = command.substr(command.find(DELIMITER) + 1);

		if (beginCommand == CONNECT_SERVER)
		{
			client->connect(endCommand);
		}
		else if (beginCommand == LIST_USERS)
		{
			client->listUsers();
		}
		else if (beginCommand == LIST_CONNECTED_USERS)
		{
			client->listOnLineUsers();
		}
		else if (beginCommand == LIST_CHAT_ROOMS)
		{
			client->listRooms();
		}else if (beginCommand == HELP)
		{
			printInstructions();
		}
		else if (beginCommand == LIST_ROOM_USERS)
		{
			client->listUsersInRoom(endCommand);
		}
		else if (beginCommand == LOGIN)
		{
			client->login(endCommand);
		}
		else if (beginCommand == REGISTER_USER)
		{
			client->regiester(endCommand);
		}
		else if (beginCommand == OPEN_CHAT)
		{
			client->openSession(endCommand);
		}
		else if (beginCommand == OPEN_ROOM)
		{
			client->openRoom(endCommand);
		}
		else if (beginCommand == SEND_MESSAGE)
		{
			client->sendMsg(endCommand);
		}
		else if (beginCommand == PRINT_STATUS)
		{
			client->currentStatus();
		}
		else if (beginCommand == CLOSE_SESSION)
		{
			client->closeSession();
		}
		else if (beginCommand == CLOSE_ROOM)
		{
			client->closeRoom(endCommand);
		}
		else if (beginCommand == DISCONNECT)
		{
			client->dissconect();
		}
		else if (beginCommand == CLOSE)
		{
			client->shutdown();
			break;
		}
		else
		{
			cout << "Error: invalid command - " << beginCommand <<endl;
		}
	}
	return 0;

}

