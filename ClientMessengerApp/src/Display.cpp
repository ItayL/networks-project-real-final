/*
 * Display.cpp
 *
 *  Created on: May 25, 2016
 *      Author: user
 */

#include "Display.h"

namespace npl {

Display::Display() {
	// TODO Auto-generated constructor stub

}

Display::~Display() {
	// TODO Auto-generated destructor stub
}

void Display::printUsersList(map<string, string> data) {
	cout<<"Users :"<<endl;
	for(map<string, string>::iterator it = data.begin(); it != data.end();++it) {
		cout<< it->second<<endl;
	}
}

void Display::printLoginError() {
	cout<<"User or Pass Incorrect"<<endl;
}

void Display::printRegisterError() {
	cout<<"User In Use"<<endl;
}

void Display::printSessionEstablished() {
	cout<<"Session Established"<<endl;
}


void Display::printSessionRefused() {
	cout<<"Session Refused"<<endl;
}

void Display::printRoomUserListUpdate() {
	cout<<"Users Room List Is updated"<<endl;
}

void Display::printARoomUserList(map<string, string> data) {
	cout<<"Room User List :"<<endl;
		for(map<string, string>::iterator it = data.begin(); it != data.end();it++) {
			cout<< it->second<<endl;
		}
}

void Display::printCommandNotFound() {
	cout<<"Command Not Found"<<endl;
}

void Display::printExitSession() {
	cout<<"Session Is Closed"<<endl;
}

void Display::printConnectedUsersList(map<string, string> data) {
	cout<<"Connected User List :"<<endl;
			for(map<string, string>::iterator it = data.begin(); it != data.end();++it) {
				cout<< it->second<<endl;
			}
}

void Display::PrintAllRooms(map<string, string> data) {
	cout<<"rooms List :"<<endl;
			for(map<string, string>::iterator it = data.begin(); it != data.end();++it) {
				cout<< it->second<<endl;
	}
}


} /* namespace npl */
