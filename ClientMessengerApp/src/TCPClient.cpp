/*
 * TCPClient.cpp
 *
 *  Created on: May 25, 2016
 *      Author: user
 */

#include "TCPClient.h"

namespace npl {

TCPClient::TCPClient(Notify * notify,string ip,int port) {
	this->notify=notify;
	this->tcpSocket = new TCPSocket(ip,port);
	//this->tcpSocket->listenAndAccept();
	this->running = true;
	start();

}
void TCPClient::close(){

}

TCPClient::~TCPClient() {
}

void TCPClient::run() {
	Command command = NONE;
	map<string, string> data;
	while (running) {
		MessengerProtocol::readMSG(tcpSocket, command, data);
		if(command!=0){
		notify->onReadCommandFromServer(this,command,data);
		}
	}
}



} /* namespace npl */

