/*
 * LoginAndRegister.h
 *
 *  Created on: May 16, 2016
 *      Author: user
 */

#ifndef LOGINANDREGISTER_H_
#define LOGINANDREGISTER_H_
#include "Dispatcher.h"
#include <fstream>
#include <stdlib.h>

using namespace std;
namespace npl{
class UsersManager : public MThread {

private:
	map<string, string> usersPasswords;
	vector<TCPSocket*> peers;
	Dispatcher * dispatcher;
	pthread_mutex_t peersLock;
	pthread_mutex_t userPasswordLock;
	bool running;

	bool Login(map<string,string>& data);
	bool Register(map<string,string> data);
	map<string, string>::iterator FindUser(string username);
	vector<TCPSocket*>::iterator findPeer(TCPSocket* peer);

public:

	UsersManager();

	Dispatcher * getDispatcher();
	void remove(TCPSocket* peer);

	void listAllRegisterUsers();
	void add(TCPSocket * peer);
	void saveToFile();
	void run();
	void shutdown();
	static map<string, string> getUserPassMap();
	virtual ~UsersManager();
};
}

#endif /* LOGINANDREGISTER_H_ */
