/*
 * Server.h
 *
 *  Created on: May 16, 2016
 *      Author: user
 */

#ifndef SERVER_H_
#define SERVER_H_

#include "UsersManager.h"


using namespace std;
namespace npl{

class Server : public MThread {

private:
	TCPSocket * socket;
	UsersManager * usersManager;

public:
	Server();
	void listAllRegisterUsers();
	void run();
	void shutdown();
	void printUsersInRoom(string roomName);
	void printOnLineUsers();
	void printActiveSessions();
	void printRooms();
	void bootstrap();
	virtual ~Server();
};

}
#endif /* SERVER_H_ */