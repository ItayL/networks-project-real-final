/*
 * Server.cpp
 *
 *  Created on: May 16, 2016
 *      Author: user
 */

#include "Server.h"
namespace npl{

Server::Server() {
	socket = new TCPSocket(MSNGR_PORT);
	loginAndRegister = new UsersManager();
	start();
}


Server::~Server() {

}


void Server::listAllRegisterUsers() {
	loginAndRegister->listAllRegisterUsers();

}

void Server::printOnLineUsers() {
	loginAndRegister->getDispatcher()->printOnLineUsers();
}

void Server::printActiveSessions() {
	loginAndRegister->getDispatcher()->printActiveSessions();
}

void Server::printRooms() {
	loginAndRegister->getDispatcher()->printAllRooms();
}

void Server::printUsersInRoom(string roomName) {
	loginAndRegister->getDispatcher()->printUsersInRoom(roomName);
}

void Server::run() {
	while(true){
		TCPSocket * peer = socket->listenAndAccept();
		if(peer == NULL){
			break;
		}
		map<string,string>portMap;
		portMap[PORT_KEY]=Global::convertTostring(peer->getDestPort());
		MessengerProtocol::SendMSG(peer,PORT_EXCHANGE,portMap);
		loginAndRegister->add(peer);
	}
}

void Server::shutdown() {
	socket->close();
	waitForThread();
	delete socket;
	loginAndRegister->shutdown();
	delete loginAndRegister;
}

}
