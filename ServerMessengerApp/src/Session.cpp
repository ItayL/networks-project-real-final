/*
 * Server.cpp
 *
 *  Created on: May 16, 2016
 *      Author: user
 */

#include "Server.h"
namespace npl{

	Server::Server() { }
	Server::~Server() { }

	void Server::bootstrap() {
		socket = new TCPSocket(MSNGR_PORT);
		usersManager = new UsersManager();
		start();
	}

	void Server::listAllRegisterUsers() {
		usersManager->listAllRegisterUsers();

	}

	void Server::printOnLineUsers() {
		usersManager->getDispatcher()->printOnLineUsers();
	}

	void Server::printActiveSessions() {
		usersManager->getDispatcher()->printActiveSessions();
	}

	void Server::printRooms() {
		usersManager->getDispatcher()->printAllRooms();
	}

	void Server::printUsersInRoom(string roomName) {
		usersManager->getDispatcher()->printUsersInRoom(roomName);
	}

	void Server::run() {
		while(true){
			TCPSocket * peer = socket->listenAndAccept();
			if(peer == NULL){
				break;
			}
			map<string,string>portMap;
			portMap[PORT_KEY]=Global::convertTostring(peer->getDestPort());
			MessengerProtocol::SendMSG(peer,PORT_EXCHANGE,portMap);
			usersManager->add(peer);
		}
	}

	void Server::shutdown() {
		socket->close();
		waitForThread();
		delete socket;
		usersManager->shutdown();
		delete usersManager;
	}

}
