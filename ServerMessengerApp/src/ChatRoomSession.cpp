/*
 * ChatRoomSession.cpp
 *
 *  Created on: May 16, 2016
 *      Author: user
 */

#include "ChatRoomSession.h"
#include "UsersManager.h"

namespace npl {


	ChatRoomSession::ChatRoomSession(Handler* handler,PeerName* peerUser, string ChatRoomName) {
		this->chatRoomOwner = peerUser;
		this->chatRoomName = ChatRoomName;
		this->chatRoomPeers.push_back(peerUser);
		this->chatActive = true;
		this->handler = handler;
		start();
	}

	string ChatRoomSession::getRoomName() {
		return this->chatRoomName;
	}


	void ChatRoomSession::printAllPeersInRoom(){
		cout<<"Users In Room"<<this->chatRoomName<<" :"<<endl;
		vector<PeerName*>::iterator it;
		for (it = this->chatRoomPeers.begin(); it != chatRoomPeers.end(); it++) {
			PeerName* peer=*it;
			cout<<peer->getUserName()<<endl;
		}
	}

	ChatRoomSession::~ChatRoomSession() {
	}

	void ChatRoomSession::run() {
		while (this->chatActive) {
			MTCPListener listener;
			listener.add(this->getPeersVector());
			TCPSocket * peer = listener.listen(30);
			if (peer != NULL) {
				Command command = NONE;
				map<string, string> data;
				MessengerProtocol::readMSG(peer, command, data);
				PeerName* sender =FindPeer(peer);
				switch (command) {

					case CLOSE_ROOM:
						if(sender == this->chatRoomOwner){
							close();
						}
					break;

					case ROOM_LIST:
							handler->onRoomUserList(peer,data);
						break;

					case LIST_USERS:
							MessengerProtocol::SendMSG(peer, LIST_USERS,UsersManager::getUserPassMap());
						break;

					case EXIT_SESSION:
							handler->onChatRoomExit(sender);
							remove(sender);
							MessengerProtocol::SendMSG(peer,EXIT_SESSION);
						break;

					case DISCONNECT:
							this->remove(sender);
							break;

					case LIST_CONNECTED_USERS:
							handler->onConnectedUserList(peer,data);
							break;

					case 0:
						this->remove(sender);
						this->update();
							break;

					default:MessengerProtocol::SendMSG(peer,COMMAND_NOT_FOUND);

				}
			}
		}
		delete this;
	}

	void ChatRoomSession::close() {
		for (vector<PeerName*>::iterator it = chatRoomPeers.begin(); it != chatRoomPeers.end();it++){
			PeerName* usr = *it;
			MessengerProtocol::SendMSG(usr->getPeer(),EXIT_ROOM);
		}
		this->chatActive=false;
		handler->onChatRoomSessionClose(this,this->chatRoomPeers);
	}


	vector<TCPSocket*> ChatRoomSession::getPeersVector() {
		vector<TCPSocket*> peers;
		for (int i = 0; i < this->chatRoomPeers.size(); i++) {
			peers.push_back(chatRoomPeers[i]->getPeer());
		}
		return peers;
	}

	void ChatRoomSession::add(PeerName* peerUser) {
		pthread_mutex_lock(&lock);
		this->chatRoomPeers.push_back(peerUser);
		pthread_mutex_unlock(&lock);
		update();

	}

	PeerName*  ChatRoomSession::FindPeer(string usrname) {
		vector<PeerName*>::iterator it;
		for (it = chatRoomPeers.begin(); it != chatRoomPeers.end();it++) {
			PeerName* usr = *it;
			if (usr->getUserName() == usrname){
				return *it;
			}
		}
		return NULL;
	}

void ChatRoomSession::remove(PeerName* peerUser) {
	pthread_mutex_lock(&lock);
	vector<PeerName*>::iterator it;
	for (it = chatRoomPeers.begin(); it != chatRoomPeers.end();it++)
	{
		if (peerUser == *it) break;
	}
	PeerName* temp=*it;
	cout<<temp->getUserName()<<" is removing from the user room list"<<endl;
	this->chatRoomPeers.erase(it);
	pthread_mutex_unlock(&lock);
	update();
}

	PeerName*  ChatRoomSession::FindPeer(TCPSocket *  peer) {
		vector<PeerName*>::iterator it;
		for (it = chatRoomPeers.begin(); it != chatRoomPeers.end();it++) {
			PeerName* usr = *it;
			if (usr->getPeer()==peer){
				return usr;
			}
		}
		return NULL;
	}

void ChatRoomSession::update() {
	map<string,string> newUserslist;
	int index=0;
	for (vector<PeerName*>::iterator it = chatRoomPeers.begin(); it != chatRoomPeers.end();it++)
	{
		string key1,key2,key3;
		PeerName* usr = *it;
		key1=IP_KEY;
		key1.append(Global::convertTostring(index).c_str());
		key2=PORT_KEY;
		key2.append(Global::convertTostring(index).c_str());
		key3=USER_NAME_KEY;
		key3.append(Global::convertTostring(index).c_str());
		newUserslist[key3]= usr->getUserName();
		newUserslist[key1]= usr->getPeer()->getDestAddr();
		newUserslist[key2]= Global::convertTostring(usr->getPeer()->getDestPort());
		index++;
	}
	for (vector<PeerName*>::iterator it = chatRoomPeers.begin(); it != chatRoomPeers.end();it++)
	{
		PeerName* usr = *it;
		MessengerProtocol::SendMSG(usr->getPeer(),UPDATE_ROOM,newUserslist);
	}
}
void ChatRoomSession::SendUserList(TCPSocket* peer){
		map<string,string> newUserslist;
		int index=0;
		newUserslist["count"]= Global::convertTostring(chatRoomPeers.size());
		for (vector<PeerName*>::iterator it = chatRoomPeers.begin(); it != chatRoomPeers.end();it++)
		{
			PeerName* usr = *it;
			string in=Global::convertTostring(index);
			newUserslist[in]=usr->getUserName();
			index++;
		}
		MessengerProtocol::SendMSG(peer,ROOM_LIST,newUserslist);
}
}


