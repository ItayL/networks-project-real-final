/*
 * CLI.h
 *
 *  Created on: Jan 13, 2017
 *      Author: user
 */

#ifndef CLI_H_
#define CLI_H_

#define LIST_USERS "lu"
#define LIST_CONNECTED_USERS "lcu"
#define LIST_SESSIONS "ls"
#define LIST_ROOMS "lr"
#define LIST_ROOM_USERS "lru"
#define SHUTDOWN "x"
#define	HELP "help"

#endif /* CLI_H_ */
