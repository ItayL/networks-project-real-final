/*
 * Dispatcher.cpp
 *
 *  Created on: May 16, 2016
 *      Author: user
 */

#include "Dispatcher.h"
#include "UsersManager.h"

namespace npl {

Dispatcher::Dispatcher() {
}

void Dispatcher::printOnLineUsers() {
	cout<<"Online users :"<<endl;
	pthread_mutex_lock(&peerlistlock);
	vector<PeerName*>::iterator it;
	for (it = peerlist.begin(); it != peerlist.end(); it++) {
		PeerName* peer=*it;
		cout<<peer->getUserName()<<endl;
	}
	pthread_mutex_unlock(&peerlistlock);
}

void Dispatcher::printActiveSessions() {
	cout<<"Active sessions :"<<endl;
	pthread_mutex_lock(&sessionslock);
	vector<Session*>::iterator it;
	for (it = sessions.begin(); it != sessions.end(); it++) {
		Session* session=*it;
		cout<<"session with" <<session->getPeerNameA()->getUserName()<<" and "<<session->getPeerNameB()->getUserName()<<endl;
	}
	pthread_mutex_unlock(&sessionslock);
}

void Dispatcher::printAllRooms() {
	cout<<"Active rooms :"<<endl;
	pthread_mutex_lock(&chatRoomslock);
	vector<ChatRoomSession* >::iterator it;
	for (it = chatRooms.begin(); it != chatRooms.end(); it++) {
		ChatRoomSession* session=*it;
		cout<<session->getRoomName()<<endl;
	}
	pthread_mutex_unlock(&chatRoomslock);
}

void Dispatcher::printUsersInRoom(string roomName) {
	ChatRoomSession* chatRoomSession = this->findChatRoom(roomName);
	chatRoomSession ->printAllPeersInRoom();
}

PeerName* Dispatcher::FindPeer(TCPSocket* sock) {
	PeerName* ret = NULL;
	pthread_mutex_lock(&peerlistlock);
	vector<PeerName*>::iterator it;
	for (it = peerlist.begin(); it != peerlist.end(); it++) {
		PeerName* usr = *it;

		if (usr->getPeer() == sock) {
			ret = *it;
		}
	}
	pthread_mutex_unlock(&peerlistlock);
	return ret;
}

PeerName* Dispatcher::FindPeer(string usrname) {
	PeerName* ret = NULL;
	pthread_mutex_lock(&peerlistlock);
	vector<PeerName*>::iterator it;
	for (it = peerlist.begin(); it != peerlist.end(); it++) {
		PeerName* usr = *it;

		if (usr->getUserName() == usrname) {
			ret = *it;
		}
	}
	pthread_mutex_unlock(&peerlistlock);
	return ret;
}

Dispatcher::~Dispatcher() {
}

void Dispatcher::add(PeerName* thisPeer) {
	pthread_mutex_lock(&peerlistlock);
	this->peerlist.push_back(thisPeer);
	pthread_mutex_unlock(&peerlistlock);
	if (peerlist.size() == 1)
		start();
}

void Dispatcher::run() {
	while (peerlist.size() > 0) {
		MTCPListener listener;
		listener.add(getPeersVector());
		TCPSocket * peer = listener.listen(30);
		if (peer != NULL) {
			Command command = NONE;
			map<string, string> data;
			MessengerProtocol::readMSG(peer, command, data);
			PeerName* peerName = FindPeer(peer);
			switch (command) {
			case OPEN_SESSION:
				this->OpenSession(peerName, data);
				break;

			case LIST_CONNECTED_USERS:
				MessengerProtocol::SendMSG(peer, LIST_CONNECTED_USERS,this->getMapOfAllConnectPeers());
				break;

			case OPEN_CHAT_ROOM:
				this->openChatRoom(peerName, data);
				break;

			case ALL_ROOMS:
				this->sendAllRoomList(data, peer);
				break;

			case LIST_USERS:
				MessengerProtocol::SendMSG(peer, LIST_USERS,UsersManager::getUserPassMap());
				break;

			case ROOM_LIST:
				this->getRoomUserList(data, peer);
				break;


			case DISCONNECT:
						removePeer(peerName);
										break;
			case NONE:
				this->removePeer(peerName);
									break;

			default:
				MessengerProtocol::SendMSG(peer, COMMAND_NOT_FOUND);

			}
		}
	}
}

void Dispatcher::OpenSession(PeerName* peerName, map<string, string> data) {
	PeerName* destUser = FindPeer(data.find(USER_NAME_KEY)->second);
	if (destUser == NULL)
		MessengerProtocol::SendMSG(peerName->getPeer(), SESSION_REFUSED);
	else {
		Session* S = new Session(this, peerName, destUser);
		pthread_mutex_lock(&sessionslock);
		sessions.push_back(S);
		pthread_mutex_unlock(&sessionslock);
		if (peerName != destUser)
			removePeer(peerName);
		removePeer(destUser);
	}
}

void Dispatcher::openChatRoom(PeerName* peerName, map<string, string> data) {
	string ChatRoomName = data.find(ROOM_NAME_KEY)->second;
	ChatRoomSession* chatRoomSession = findChatRoom(ChatRoomName);
	if (chatRoomSession == NULL) {
		ChatRoomSession* newChatRoomSession = new ChatRoomSession(this,peerName,ChatRoomName);
		pthread_mutex_lock(&chatRoomslock);
		chatRooms.push_back(newChatRoomSession);
		pthread_mutex_unlock(&chatRoomslock);
		this->removePeer(peerName);
	} else {
		chatRoomSession->add(peerName);
		this->removePeer(peerName);
	}
	MessengerProtocol::SendMSG(peerName->getPeer(),ROOM_SESSION_ESTABLISHED);
}

ChatRoomSession* Dispatcher::findChatRoom(string usrname) {
	vector<ChatRoomSession*>::iterator it;
	ChatRoomSession* ret = NULL;
	pthread_mutex_lock(&chatRoomslock);
	for (it = chatRooms.begin(); it != chatRooms.end(); it++) {
		ChatRoomSession* room = *it;
		if (room->getRoomName() == usrname) {
			ret = room;
		}
	}
	pthread_mutex_unlock(&chatRoomslock);
	return ret;
}

void Dispatcher::removePeer(PeerName* peerName) {
	vector<PeerName*>::iterator it;
	pthread_mutex_lock(&peerlistlock);
	for (it = peerlist.begin(); it != peerlist.end(); it++) {
		PeerName* usr = *it;
		if (usr->getPeer() == peerName->getPeer())
			break;
	}
	if (it != peerlist.end()) {
		peerlist.erase(it);
	}
	pthread_mutex_unlock(&peerlistlock);
}

vector<TCPSocket*> Dispatcher::getPeersVector() {

	vector<TCPSocket*> ret;
	pthread_mutex_lock(&peerlistlock);
	for (int i = 0; i < peerlist.size(); i++) {
		ret.push_back(peerlist[i]->getPeer());
	}
	pthread_mutex_unlock(&peerlistlock);
	return ret;
}

void Dispatcher::onChatRoomSessionClose(ChatRoomSession* brocker,vector<PeerName*> chatRoomPeers) {
	for (vector<PeerName*>::iterator it = chatRoomPeers.begin();it != chatRoomPeers.end(); it++) {
		PeerName* usr = *it;
		this->add(usr);
	}
	pthread_mutex_lock(&chatRoomslock);
	for (vector<ChatRoomSession*>::iterator it = chatRooms.begin();it != chatRooms.end(); it++)
	{
		ChatRoomSession* room = *it;
		if (room == brocker) {
			chatRooms.erase(it);
			break;
		}
	}
	pthread_mutex_unlock(&chatRoomslock);

}
void Dispatcher::onChatRoomExit(PeerName* chatRoomPeer) {
	this->add(chatRoomPeer);
}

void Dispatcher::onSessionClose(Session* brocker, PeerName* peerNameA,PeerName* peerNameB){
	if (peerNameA!=NULL)
		this->add(peerNameA);
	if (peerNameB!=NULL)
		this->add(peerNameB);
	pthread_mutex_lock(&sessionslock);
	for (vector<Session*>::iterator it = sessions.begin(); it != sessions.end();it++) {
		Session* session = *it;
		if (session == brocker) {
			sessions.erase(it);
			break;
		}
	}
	pthread_mutex_unlock(&sessionslock);

}

vector<PeerName*>& Dispatcher::getPeerlist() {
	return peerlist;
}

map<string, string> Dispatcher::getMapOfAllConnectPeers() {
	map<string, string> usrList;
	int i = 0;
	pthread_mutex_lock(&peerlistlock);
	for (vector<PeerName*>::iterator it = peerlist.begin();it != peerlist.end(); ++it) {
		PeerName* usr = *it;
		string userName = usr->getUserName();
		string UserIndex = Global::convertTostring(i);
		usrList.insert(pair<string, string>(UserIndex, userName));
		i++;
	}
	pthread_mutex_unlock(&peerlistlock);
	return usrList;

}

void Dispatcher::onRoomUserList(TCPSocket * peer, map<string, string> data) {
	this->getRoomUserList(data, peer);
}

void Dispatcher::getRoomUserList(map<string, string> data, TCPSocket* peer) {
	string ChatRoomName = data.find(ROOM_NAME_KEY)->second;
	ChatRoomSession* theRoom = this->findChatRoom(ChatRoomName);
	if (theRoom != NULL) {
		theRoom->SendUserList(peer);
	} else
	{
		MessengerProtocol::SendMSG(peer,ERROR);

	}
}

void Dispatcher::sendAllRoomList(map<string, string> data, TCPSocket* peer) {
	map<string, string> roomList;
	int i = 0;
	pthread_mutex_lock(&chatRoomslock);
	for (vector<ChatRoomSession*>::iterator it = chatRooms.begin();it != chatRooms.end(); ++it) {
		ChatRoomSession* crm = *it;
		string roomIndex = Global::convertTostring(i);
		roomList.insert(pair<string, string>(roomIndex, crm->getRoomName()));
		i++;
	}
	pthread_mutex_unlock(&chatRoomslock);
	MessengerProtocol::SendMSG(peer, ALL_ROOMS, roomList);
}

void Dispatcher::onConnectedUserList(TCPSocket* peer,map<string,string> data){
	MessengerProtocol::SendMSG(peer, LIST_CONNECTED_USERS,this->getMapOfAllConnectPeers());
}


}
