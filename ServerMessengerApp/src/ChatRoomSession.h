/*
 * ChatRoomSession.h
 *
 *  Created on: May 16, 2016
 *      Author: user
 */

#ifndef CHATROOMSESSION_H_
#define CHATROOMSESSION_H_

#include "MThread.h"
#include <TCPSocket.h>
#include "MTCPListener.h"
#include <vector>
#include "PeerName.h"
#include <MessengerProtocol.h>
#include "Global.h"
#include <map>

using namespace std;

namespace npl{

	class ChatRoomSession : public MThread{
	public:
		class Handler{
		public:
			virtual void onChatRoomSessionClose(ChatRoomSession* brocker,vector<PeerName*> chatRoomPeers)=0;
			virtual void onChatRoomExit(PeerName* chatRoomPeer)=0;
			virtual void onRoomUserList(TCPSocket* peer,map<string,string> data)=0;
			virtual void onConnectedUserList(TCPSocket* peer,map<string,string> data)=0;
		};
	private:
		vector<PeerName*> chatRoomPeers;
		string chatRoomName;
		PeerName * chatRoomOwner;
		bool chatActive;
		Handler* handler;
		pthread_mutex_t lock;

	public:

		ChatRoomSession(Handler* handler,PeerName* peerUser, string ChatRoomName);
		string getRoomName();
		void run();
		void close();
		virtual ~ChatRoomSession();
		vector<TCPSocket*> getPeersVector();
		void add(PeerName * peerUser);
		void remove(PeerName * peerUser);
		PeerName* FindPeer(string usrname);
		PeerName* FindPeer(TCPSocket *  peer);
		void update();
		void SendUserList(TCPSocket* peer);
		map<string,string> getUserListInRoomMap();
		void printAllPeersInRoom();

	 vector<PeerName*> getChatRoomPeers()  {
		return chatRoomPeers;
	}
};
}
#endif /* CHATROOMSESSION_H_ */
