/*
 * LoginAndRegister.cpp
 *
 *  Created on: May 16, 2016
 *      Author: user
 */

#include "UsersManager.h"

namespace npl {

#define USER_PASS_FILE "UserPass.upf"

void UsersManager::listAllRegisterUsers() {
	pthread_mutex_lock(&userPasswordLock);
	for (map<string, string>::iterator it = usersPasswords.begin();
			it != usersPasswords.end(); it++) {
		cout << it->first << endl;
	}
	pthread_mutex_unlock(&userPasswordLock);
}

static map<string, string> LoadUserPassFile() {
	map<string, string> userPassMap;
	ifstream file(USER_PASS_FILE, ios::in);
	if (file.is_open()) {
		string size, buffer, username, password;
		getline(file, size);
		int Mapsize = atoi(size.c_str());
		for (int i = 0; i < Mapsize; i++) {
			getline(file, buffer);
			username = buffer;
			getline(file, buffer);
			password = buffer;
			userPassMap[username] = password;
		}
		cout << "Finish load process of the registered user list" << endl;
		file.close();
	}
	return userPassMap;
}

map<string, string> UsersManager::getUserPassMap() {
	map<string, string> userPassMap = LoadUserPassFile();
	map<string, string> ret;
	int i = 0;
	for (map<string, string>::iterator it = userPassMap.begin();
			it != userPassMap.end(); ++it) {
		string userName = it->first;
		string UserIndex = Global::convertTostring(i);
		ret[UserIndex] = userName;
		i++;
	}
	return ret;

}

static void SaveMap(map<string, string>* outputMap) {
	ofstream file(USER_PASS_FILE, ios::out | ios::trunc);
	if (file.is_open()) {
		file << outputMap->size() << "\n";
		for (map<string, string>::iterator it = outputMap->begin();
				it != outputMap->end(); it++) {
			file << it->first << endl;
			file << it->second << endl;
		}
		file.close();
	} else
		cout << "ERROR: Unable to open UserPass file" << endl;
}


UsersManager::UsersManager() {

	dispatcher = new Dispatcher();
	this->usersPasswords = LoadUserPassFile();

}

void UsersManager::add(TCPSocket* peer) {
	pthread_mutex_lock(&peersLock);
	peers.push_back(peer);
	if (peers.size() == 1) {
		start();
	}
	pthread_mutex_unlock(&peersLock);
}

void UsersManager::remove(TCPSocket* peer) {
	pthread_mutex_lock(&peersLock);
	vector<TCPSocket*>::iterator it = findPeer(peer);
	if (it != peers.end())
		peers.erase(it);
	else
		cout << "Can't remove peer(peer not found)" << endl;
	pthread_mutex_unlock(&peersLock);
}

vector<TCPSocket*>::iterator UsersManager::findPeer(TCPSocket* peer) {
	if (peers.size() > 0) {
		vector<TCPSocket*>::iterator it;
		for (it = peers.begin(); it != peers.end(); it++) {
			if (*it == peer) {
				return it;
			}
		}
	}
	return peers.end();
}

void UsersManager::run() {
	while (peers.size() > 0) {
		MTCPListener listener;
		listener.add(this->peers);
		TCPSocket * peer = listener.listen(30);

		if (peer != NULL) {
			Command command;
			map<string, string> data;
			MessengerProtocol::readMSG(peer, command, data);

			switch (command) {

			case LIST_USERS:

				MessengerProtocol::SendMSG(peer, LIST_USERS,
						getUserPassMap());
				break;

			case REGISTER:
				if (this->Register(data)) {
					PeerName * thisPeer = new PeerName(
							data.find(USER_NAME_KEY)->second, peer);
					dispatcher->add(thisPeer);
					this->remove(peer);
				} else {
					MessengerProtocol::SendMSG(peer, ERROR_REGISTER);

				}
				break;

			case LOGIN:
				if (this->Login(data)) {
					PeerName * thisPeer = new PeerName(
							data.find(USER_NAME_KEY)->second, peer);
					dispatcher->add(thisPeer);
					this->remove(peer);
					cout << "login successful" << endl;
				} else {
					MessengerProtocol::SendMSG(peer, ERROR_LOGIN);
				}
				break;

			case LIST_CONNECTED_USERS:

				MessengerProtocol::SendMSG(peer, LIST_CONNECTED_USERS,
						this->dispatcher->getMapOfAllConnectPeers());

				break;
			case DISCONNECT:
				this->remove(peer);
				break;
			case NONE:
				this->remove(peer);
				break;
			default:
				MessengerProtocol::SendMSG(peer, COMMAND_NOT_FOUND);
				break;
			}
		}
	}
}

bool UsersManager::Login(map<string, string>& data) {
	bool ret = false;
	string userName = data.find(USER_NAME_KEY)->second;
	string password = data.find(PASSWORD_KEY)->second;

	pthread_mutex_lock(&userPasswordLock);
	map<string, string>::iterator iter = FindUser(userName);
	if (iter != usersPasswords.end()) {
		if (password.compare((string) iter->second) == 0) {
			ret = true;
		}
	}
	pthread_mutex_unlock(&userPasswordLock);
	return ret;
}

bool UsersManager::Register(map<string, string> data) {
	string userName = data.find(USER_NAME_KEY)->second;
	string password = data.find(PASSWORD_KEY)->second;
	cout << userName << ":" << password << endl;

	bool ret = false;
	pthread_mutex_lock(&userPasswordLock);
	if (this->FindUser(userName) == usersPasswords.end()) {
		usersPasswords[userName] = password;
		saveToFile();
		ret = true;
	}
	pthread_mutex_unlock(&userPasswordLock);
	return ret;
}

map<string, string>::iterator UsersManager::FindUser(string username) {
	map<string, string>::iterator it = usersPasswords.find(username);
	return it;
}

void UsersManager::shutdown() {
	pthread_mutex_lock(&peersLock);
	for (vector<TCPSocket*>::iterator it = peers.begin(); it != peers.end();
			++it) {
		TCPSocket* p = *it;
		p->close();
		delete p;
	}
	pthread_mutex_unlock(&peersLock);
	delete dispatcher;
}

Dispatcher * UsersManager::getDispatcher() {
	return dispatcher;
}

void UsersManager::saveToFile() {
	SaveMap(&usersPasswords);
}

UsersManager::~UsersManager() {
}

}
