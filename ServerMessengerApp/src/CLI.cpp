/*
 * CLI.h
 *
 *  Created on: Jan 13, 2017
 *      Author: user
 */

#include <iostream>
#include <string.h>
#include "Server.h"
#include "CLI.h"
#include <Global.h>

using namespace std;
using namespace npl;

static void printInstructions(){
		cout<<"---------------MESSENGER SERVER---------------"<<endl;
		cout<<"lu ­             - List Registered Users"<<endl;
		cout<<"lcu             ­- List Connected Users  "<<endl;
		cout<<"ls ­             - List Chat Sessions"<<endl;
		cout<<"lr ­             - List Room Sessions"<<endl;
		cout<<"lru <room name> - ­List Users In Room"<<endl;
		cout<<"x               - Shutdown"<<endl;
		cout<<"help 		   - Print Help Menu"<<endl;
		cout << "-----------------------------------------" << endl;
}

int main() {
	cout << "Welcome to TCP Messenger Server" << endl;
	Server* server = new Server();
	printInstructions();
	while (true) {
		string msg;
		string command;
		cin >> command;

		string delimiter = ":";
		string beginCommand = command.substr(0, command.find(delimiter));
		string endCommand = command.substr(command.find(delimiter)+1);

		if (beginCommand == LIST_USERS)
		{
			server->listAllRegisterUsers();
		}
		else if (beginCommand == LIST_CONNECTED_USERS)
		{
			server->printOnLineUsers();
		}
		else if (beginCommand == LIST_SESSIONS)
		{
			server->printActiveSessions();
		}
		else if (beginCommand == LIST_ROOMS)
		{
			server->printRooms();
		}
		else if (beginCommand == LIST_ROOM_USERS)
		{
			server->printUsersInRoom(endCommand);
		}
		else if (beginCommand == HELP)
		{
			printInstructions();
		}
		else if (beginCommand == SHUTDOWN)
		{
			server->shutdown();

			break;
		}
		else
		{
			cout << "Error: invalid command - " << beginCommand <<endl;
		}
	}
	return 0;
}
