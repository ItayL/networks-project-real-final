/*
 * Dispatcher.h
 *
 *  Created on: May 16, 2016
 *      Author: user
 */

#ifndef DISPATCHER_H_
#define DISPATCHER_H_

#include "Session.h"
#include "ChatRoomSession.h"


using namespace std;
namespace npl{
class Dispatcher : public MThread, public ChatRoomSession::Handler, public Session::Handler {

	vector<PeerName*> peerlist;
	vector<Session*> sessions;
	vector<ChatRoomSession*> chatRooms;
	pthread_mutex_t peerlistlock;
	pthread_mutex_t sessionslock;
	pthread_mutex_t chatRoomslock;

public:

	Dispatcher();


	void openChatRoom(PeerName* peerName,map<string,string> data);
	void printOnLineUsers();
	ChatRoomSession* findChatRoom(string usrname);
	void removePeer(PeerName* peerName);
	void add(PeerName *thisPeer);
	void printAllRooms();
	void OpenSession(PeerName* sock,map<string,string> data);
	void printActiveSessions();
	PeerName* FindPeer(TCPSocket* sock);
	PeerName* FindPeer(string usrname);
	void run();
	vector<TCPSocket*> getPeersVector();
	void printUsersInRoom(string roomName);
	virtual ~Dispatcher();
	void getRoomUserList(map<string, string> data,TCPSocket* peer);
	void sendAllRoomList(map<string, string> data,TCPSocket* peer);
	vector<PeerName*>& getPeerlist();
	map<string,string> getMapOfAllConnectPeers();

	void onChatRoomExit(PeerName* chatRoomPeer);
	void onSessionClose(Session* brocker,PeerName* peerUserA,PeerName* peerUserB);
	void onConnectedUserList(TCPSocket* peer,map<string,string> data);
	void onRoomUserList(TCPSocket * peer,map<string, string> data);
	void onChatRoomSessionClose(ChatRoomSession* brocker,vector<PeerName*> chatRoomPeers);

};
};
#endif /* DISPATCHER_H_ */
