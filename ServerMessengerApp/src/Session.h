/*
 * Session.h
 *
 *  Created on: May 16, 2016
 *      Author: user
 */

#ifndef SESSION_H_
#define SESSION_H_
#include "MThread.h"
#include <TCPSocket.h>
#include "MTCPListener.h"
#include <vector>
#include "PeerName.h"
#include <MessengerProtocol.h>
#include "Global.h"


using namespace std;

namespace npl {

	class Session: public MThread {
	public:
		class Handler{
		public:
			virtual void onSessionClose(Session* brocker,PeerName* peerUserA,PeerName* peerUserB)=0;
			virtual void onConnectedUserList(TCPSocket* peer,map<string,string> data)=0;
		};
	private:
		PeerName* peerUserA;
		PeerName* peerUserB;
		Handler* handler;
		bool isActive;
		pthread_mutex_t lock;
	public:
		Session(Handler* handler ,PeerName* peerUserA, PeerName* peerUserB);
		void close();
		void run();
		virtual ~Session();
		PeerName* FindPeer(TCPSocket* peer);
		void update();

	 PeerName* getPeerNameA()  {
		return peerUserA;
	}


	 PeerName* getPeerNameB()  {
		return peerUserB;
	}


};
}
#endif /* SESSION_H_ */
