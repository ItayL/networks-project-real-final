/*
 * PeerUser.h
 *
 *  Created on: May 16, 2016
 *      Author: user
 */

#ifndef PEERUSER_H_
#define PEERUSER_H_

#include <string>
#include <iostream>
#include "TCPSocket.h"

using namespace std;
namespace npl{
class PeerName {
private:
	TCPSocket* peerSock;
	string userName;
public:
	PeerName(string userName,TCPSocket* peerSock);
	virtual ~PeerName();

	TCPSocket* getPeer();
	string getUserName();

};
}
#endif /* PEERUSER_H_ */
